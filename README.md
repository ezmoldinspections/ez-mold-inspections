EZ Mold Inspections provides mold inspections, mold testing and asbestos testing services and serves communities in San Diego and Riverside Counties. We specialize in inspecting and testing residential properties for mold problems. We've worked with a wide range of clients including: homeowners, tenants, renters, landlords, property managers, real estate agents and realtors. You'll have peace of mind because you'll receive honest answers about potential mold problems in your home.

I'm Robert Armstrong, owner of EZ Mold Inspections. I have more than two decades of experience as a real estate and mold inspector in Southern California. Along with my expertise, I'm known for genuinely caring about my customers, providing honest answers, and utilizing ethical business practices. In addition, I can accurately interpret mold testing results while many mold inspectors cannot. I look forward to serving you.

Website: https://www.ezmoldinspections.com
